# README

## Entidades

- Filmes
  - Título: String
  - URL da imagem de capa: String
  - Sinopse: String
  - Gênero: String
  - ClassificaçÃo Indicativa: Integer
    - De 0 a 5
  - Diretor: String
  - **Sessões** (1:N)
  
- Sessões
  - Horários: String
    - Formato: "hh:mm"
    - Cidade: String
    - Bairro: String
    - Tipo: Integer
      - De 0 a 2 (2D, 3D e IMAX)
    - **Filme** (N:1)
    - **Assentos** (1:N)

- Assentos
  - Números: Integer
    - De 1 a 18
  - Fileira: String
    - De "A" a "J"
  - Preço: Float
  - CPF do ocupante: String
  - Nome do ocupante: String

- Usuários
  - Nome: String
  - Sobrenome: String
  - CPF: String
    - Formato "00000000000"
  - Data de Nascimento: String
  - Nome de Usuário: String
  - E-mail: String
  - Senha: String

## Requisitos

- [ ] Leitura de Filmes
- [ ] Leitura de Sessões
- [ ] Criação automática de assentos para cada sessão criada
  - Obs: Assentos númericos de 1 a 18, com fileiras de "A" a "J"
- [ ] **BÔNUS**: Criação de um usuário
- [ ] **BÔNUS**: Login de usuário cadastrado
  
## Regras

- Filtrar Filmes por "classificação" e/ou "gênero"
- Cada sessão deve se associar a um filme e a X assentos obrigatoriamente

## Tarefas

- [ ] Criar usuários
- [ ] Buscar usuário cadastrado (Login)
- [X] Criar filmes
- [ ] Buscar filme (Barra de pesquisa)
- [X] Filtrar filmes por classificação e/ou gênero
- [ ] Criar sessões
- [ ] Filtrar por cidade e bairro
- [ ] Filtrar por tipo de sessão (2D, 3D e IMAX)
- [ ] Criar assentos da sessão selecionada
