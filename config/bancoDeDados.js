const { Sequelize } = require("sequelize");

const sequelize = new Sequelize({
    "dialect": "sqlite",
    "storage": "./data/database.sqlite",
});

async function testarConexaoBancoDeDados(){
    try {
        await sequelize.authenticate();  
    } catch (error) {
        console.log("Não foi possível se conectar ao Banco de Dados");
    }
}

testarConexaoBancoDeDados();

module.exports = sequelize;