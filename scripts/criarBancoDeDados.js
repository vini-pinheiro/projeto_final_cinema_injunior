const sequelize = require("../config/bancoDeDados");

async function criarBancoDeDados() {
    try {
        await sequelize.sync({alter: true});
        console.log("Banco de Dados criado com sucesso!")
    } catch (error) {
        console.log("Erro na criação do Banco de dados: ", error);
    }
}

module.exports = criarBancoDeDados;