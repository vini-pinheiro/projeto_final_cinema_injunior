const { Op } = require("sequelize");
const { Filmes } = require("../models/models");

//Criar um filme
async function criarFilme(request, response) {
    try {
        const {titulo, urlImagemCapa, sinopse, genero, classificacao, diretor} = request.body;

        const filme = await Filmes.create({
            titulo, 
            urlImagemCapa, 
            sinopse, 
            genero, 
            classificacao, 
            diretor
        })

        return response
            .status(201)
            .json(filme.toJSON());
    } catch (error) {
        return response
            .status(400)
            .json({error: "Não foi possível criar o filme!"});
    }
    
}

// Filtrar todos os filmes, filtrar pelo query params a classificação e/ou gênero
async function filtrarFilmes (request, response) {
    try {
        const {classificacao, genero} = request.query;
        
        //Filtrar todos os filmes
        if(!classificacao && !genero) 
        {
            const filmes = await Filmes.findAll();

            return response
                .status(200)
                .json(filmes);
        }
        //Filtrar todos os filmes pela classificação e pelo gênero
        else if (classificacao && genero) 
        {
            const filmes = await Filmes.findAll({
                where: {
                    [Op.and]: {
                        classificacao: classificacao,
                        genero: genero
                    }
                }
            });

            return response
            .status(200)
            .json(filmes);
        }
        //Filtrar todos os filmes pela classificação
        else if (classificacao) 
        {
            const {classificacao} = request.query;
            const filmes = await Filmes.findAll({
                where: {
                    [Op.and]: {
                        classificacao: classificacao
                    }
                }
            }); 
            
            return response
            .status(200)
            .json(filmes);
        }
        //Filtrar todos os filmes pelo gênero
        else if (genero)
        {
            const {genero} = request.query;
            const filmes = Filmes.findAll({
                where: {
                    [Op.and]: {
                        genero: genero
                    }
                }
            });

            return response
            .status(200)
            .json(filmes);
            }

    } catch (error) {
        return response
            .status(500)
            .json({error: "Erro ao listar filmes!"});
    }
    
}

//Buscar filme pela barra de pesquisa
function buscarFilmeBarraDePesquisa(request, response) {
    const filme = request.filme;

    return response
        .status(200)
        .json(filme);

}

module.exports = {
    criarFilme,
    filtrarFilmes,
    buscarFilmeBarraDePesquisa,
}