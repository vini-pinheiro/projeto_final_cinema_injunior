const { Filmes } = require("../models/models");

async function buscarFilme(request, response, next) {
    try {
        //Encontrar filme
        const { id } = request.params;

        const filme = await Filmes.findByPk(id);

        if(!filme) return response
                    .status(404)
                    .json({error: "Filme não encontrado!"});

        request.filme = filme;

        return next();
    } catch (error) {
        return response
            .status(500)
            .json({error: "Erro ao tentar achar o filme"});
    }
    
}

module.exports = buscarFilme;