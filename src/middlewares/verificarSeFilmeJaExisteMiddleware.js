const { Filmes } = require("../models/models");

async function verificarSeFilmeJaExiste(request, response, next) {
    try {
        const { titulo } = request.body;

        const filmeJaFoiCriado = await Filmes.findOne({
            where: {
                titulo,
            },
        });

        if (filmeJaFoiCriado) {
            return response
                .status(400)
                .json({error: "Filme já foi criado!"});
        }

        return next();
    } catch (error) {
        return response
            .status(500)
            .json({error: "Erro ao checar se o filme já existe"});
    }
    
}

module.exports = verificarSeFilmeJaExiste;