const { DataTypes } = require("sequelize");
const { v4:uuidv4 } = require("uuid");

const sequelize = require("../../config/bancoDeDados");

const Filmes = sequelize.define("Filmes", {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: () => uuidv4(),
        primaryKey: true,
    },
    titulo: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    urlImagemCapa: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    sinopse: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    genero: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    classificacao: {
        type: DataTypes.INTEGER,
        allowNull: false,
    }, 
    diretor: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});

const Sessoes = sequelize.define("Sessoes", {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: () => uuidv4(),
        primaryKey: true,
    },
    horarios: {
        //formato - hh:mm
        type: DataTypes.STRING,
        allowNull: false
    },
    cidade: {
        type: DataTypes.STRING,
        allowNull: false
    },
    bairro: {
        type: DataTypes.STRING,
        allowNull: false
    },
    tipo: {
        //de 0 a 2 (2D, 2D e IMAX)
        type: DataTypes.INTEGER,
        allowNull: false    
    }
});

const Assentos = sequelize.define("Assentos", {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: () => uuidv4(),
        primaryKey: true,
    },
    numero: {
        // de 1 a 18
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    fileira: {
        type: DataTypes.STRING,
        allowNull: false
    },
    preco: {
        type: DataTypes.FLOAT,
        allowNull: false
    },
    cpfDoOcupante: {
        // Formato "00000000000"
        type: DataTypes.STRING,
        allowNull: false
    },
    nomeDoOcupante: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

const Usuarios = sequelize.define("Usuarios", {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: () => uuidv4(),
        primaryKey: true,
    },
    nome: {
        type: DataTypes.STRING,
        allowNull: false
    },
    sobrenome: {
        type: DataTypes.STRING,
        allowNull: false
    },
    //Formato "00000000000"
    cpf: {
        type: DataTypes.STRING,
        allowNull: false
    },
    dataDeNascimento: {
        type: DataTypes.STRING,
        allowNull: false
    },
    nomeDeUsuario: {
        type: DataTypes.STRING,
        get(){
            const rawValue = this.getDataValue("nomeDeUsuario");
            return rawValue ? rawValue.toUpperCase() : null;
        },
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    senha: {
        type: DataTypes.STRING,
        set(value){
            this.setDataValue("senha", hash(value));
        },
        allowNull: false
    },
});


Filmes.hasMany(Sessoes, { onDelete: "CASCADE", foreignKey:"FilmesId"});
Sessoes.belongsTo(Filmes, {foreignKey: "FilmesId"});

Sessoes.hasMany(Assentos, {onDelete: "CASCADE"});

module.exports = {
    Filmes,
    Sessoes,
    Assentos,
    Usuarios
}