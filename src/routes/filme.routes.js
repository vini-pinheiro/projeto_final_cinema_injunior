const express = require("express");

const filmeRoutes = express.Router();

//Inclusão dos middlewares
const verificarSeFilmeJaExiste = require("../middlewares/verificarSeFilmeJaExisteMiddleware");
const buscarFilme = require("../middlewares/buscarFilmesMiddleware");

//Inclusão dos Controllers
const filmeController = require("../controllers/filmeController");

// Criar filmes
filmeRoutes.post("/", verificarSeFilmeJaExiste, (request, response) => filmeController.criarFilme(request, response));

// Filtrar todos os filmes, filtrar pela classificação e/ou gênero
filmeRoutes.get("/", (request, response) => filmeController.filtrarFilmes(request, response));

//Buscar filme pela barra de pesquisa
filmeRoutes.get("/:id", buscarFilme, (request, response) => filmeController.buscarFilmeBarraDePesquisa(request, response));

module.exports = filmeRoutes;