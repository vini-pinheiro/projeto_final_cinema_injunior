const Router = require("express");

const filmeRoutes = require("./filme.routes");

const router = Router();

router.use("/api/filmes", filmeRoutes);

module.exports = router;