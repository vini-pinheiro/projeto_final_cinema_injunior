const express = require("express");
const swaggerUi = require("swagger-ui-express");
const app = express();

const port = 3333;

const router = require("./routes/index");
const swaggerFile = require("./swagger.json");
const criarBancoDeDados = require("../scripts/criarBancoDeDados");

app.use(express.json());

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.use(router);

app.listen(port, ()=>{
    console.log("Servido funcionando na porta: ", port,"\nCTRL + C para encerrar.");
});

criarBancoDeDados();